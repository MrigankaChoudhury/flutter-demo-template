import 'package:capapp/utils/constant.dart';
import 'package:flutter/material.dart';
import 'package:capapp/utils/data.dart';

class Quest extends StatefulWidget {
  const Quest({super.key});

  @override
  State<Quest> createState() => _QuestState();
}

class _QuestState extends State<Quest> {
  Map<String, dynamic> color = {
    "Daily Quest": secondaryColor,
    "Other Quest": primaryColor,
  };
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height * 0.2;
    final width = MediaQuery.of(context).size.height * 0.2;
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              const Padding(
                padding:
                    EdgeInsets.only(top: 20, left: 10, right: 10, bottom: 20),
                child: Text(
                  "Quest Hub",
                  style: TextStyle(fontSize: 20, color: primaryColor),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      ...questType.map(
                        (e) => Card(
                          color: color[e['card']],
                          elevation: 4,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          child: SizedBox(
                            height: height,
                            width: width,
                            child: Center(
                              child: Text(e['card']),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Column(
                children: [
                  ...quest.map(
                    (e) => Card(
                      elevation: 4,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListTile(
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (context) {
                                  return const AlertDialog(
                                    title:
                                        Center(child: Text('Write an Artical')),
                                  );
                                });
                            // showModalBottomSheet(
                            //     context: context,
                            //     builder: (context) {
                            //       return SizedBox(
                            //         height:
                            //             MediaQuery.of(context).size.height * 0.2,
                            //         child: const Padding(
                            //           padding: EdgeInsets.all(8.0),
                            //           child: Card(
                            //             child: Text("hello"),
                            //           ),
                            //         ),
                            //       );
                            //     });
                          },
                          title: Text(e['Quest']),
                        ),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

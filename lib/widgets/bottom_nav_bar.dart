import 'package:capapp/features/home/home.dart';
import 'package:capapp/features/quest/quest_hub.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:capapp/utils/constant.dart';

class BottomNavBar extends StatefulWidget {
  const BottomNavBar({super.key});

  @override
  State<BottomNavBar> createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int currentIndex = 1;
  Widget defaultBody = const Quest();
  List body = [
    {"home": const Home()},
    {"home": const Quest()},
    {"home": const Quest()},
    {"home": const Quest()},
    {"home": const Home()},
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFEEEEEE),
      body: defaultBody,
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: primaryColor,
        ),
        child: BottomNavigationBar(
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.grey,
          showUnselectedLabels: false,
          elevation: 0,
          currentIndex: currentIndex,
          onTap: (val) {
            setState(() {
              currentIndex = val;
              defaultBody = body[val]["home"];
            });
          },
          items: const [
            BottomNavigationBarItem(
                icon: FaIcon(
                  FontAwesomeIcons.home,
                  size: 20,
                ),
                label: "Home"),
            BottomNavigationBarItem(
                icon: FaIcon(
                  FontAwesomeIcons.message,
                  size: 20,
                ),
                label: "Quest"),
            BottomNavigationBarItem(
                icon: FaIcon(
                  FontAwesomeIcons.list,
                  size: 20,
                ),
                label: "List"),
            BottomNavigationBarItem(
                icon: FaIcon(
                  FontAwesomeIcons.bell,
                  size: 20,
                ),
                label: "Notification"),
            BottomNavigationBarItem(
                icon: FaIcon(
                  FontAwesomeIcons.user,
                  size: 20,
                ),
                label: "User"),
          ],
        ),
      ),
    );
  }
}
